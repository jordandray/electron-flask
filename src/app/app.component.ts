import { Component, OnInit } from '@angular/core';
import { TestService } from './service/test.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'electron-flask';
  serverMessage: Observable<string> = null;

  constructor(private testService: TestService) { }

  ngOnInit() {
    // Give the web service time to start; May need to have loading splash while this occurs
    setTimeout( () => {
      this.serverMessage = this.testService.getIndex();
    }, 2000);
  }
}
