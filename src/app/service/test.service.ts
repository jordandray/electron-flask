import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class TestService {

  constructor(private http: HttpClient) { }

  getIndex(): Observable<string> {
    return this.http.get<{ message: string }>('http://localhost:5000/').pipe(
      map(res => res.message)
    );
  }
}
