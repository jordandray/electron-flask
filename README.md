# ElectronFlask

## Running the Application

The Python web Service must be built and packaged using `pyinstaller` before the electron app is built and run. Alternatively the web Services
could be run in python and `ng serve` could be used to serve the angular app at `http://localhost:4200` for development.

### Building the Web Service

NOTE: `pyinstaller` has an issue using `setuptools` when a version greater than `45.0.0` is used. The requirements.txt for the `webservice` project
uses `pyinstaller` at version `3.6` and `setuptools` at version `44.1.1`. If you get errors trying to run the web services after using `pyinstaller`
to package them please check the versions used.

To build and package the web services, run `pyinstaller -F serve.py` while in the `/webservice` directory.

### Running the Electron Application

The electron application can be be run with `npm run electron`. Please note this will also run the `webservice` project built in the previous step.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

