from app import app
from flask import request


@app.route('/')
@app.route('/index', methods=['GET'])
def get_test():
    return "{ \"message\": \"Information back from python land!\" }"


@app.route('/postParamsTest/<params>', methods=['POST'])
def post_params_test(params):
    return params


@app.route('/postBodyTest', methods=['POST'])
def post_body_test():
    return request.data
