from gevent.pywsgi import WSGIServer
import gevent
import signal
from app import app

http_server = WSGIServer(('', 5000), app)

if __name__ == "__main__":
    # Catch SIGINT and stop server
    gevent.signal_handler(signal.SIGINT, http_server.stop)
    http_server.serve_forever()
